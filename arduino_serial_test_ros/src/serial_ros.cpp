//#include "arduino_node.hpp"
#include <string>
#include <exception>
#include <cmath>
#include <iostream>

#include <ros/ros.h>
#include <serial/serial.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>

union Pion_protocol {
    float speeds[2];
    unsigned char  transfer[8];
};

const float speed_epsilon   = 1e-1;
const float wheel_radius    = 0.11; // maybe in future import from urdf, now it`s from manual check
const float base_width      = 0.4;  // too

const float max_robot_speed = 0.2;

class Pion_arduino_node {
    serial::Serial ser;
    Pion_protocol pion;
    
    ros::NodeHandle nh;
    ros::Subscriber sub;
    ros::Publisher  pub;
    
    void write_to_serial(float left, float right);
    void read_from_serial(float &left, float &right);
    
    void speeds_parser_to_ros(float &left, float &right);
    void speeds_parser_to_ard(float &lin, float &ang);
    
public:
    
    void speeds_deliver_cb(const geometry_msgs::Twist::ConstPtr& msg);
    void check_serial();
    Pion_arduino_node(std::string port_name, std::string sub_topic, std::string pub_topic);
};

Pion_arduino_node::Pion_arduino_node(std::string port_name, std::string sub_topic, std::string pub_topic) {
    pion.speeds[0] = 0;
    pion.speeds[1] = 0;
    
    ser.setPort(port_name); 
    ser.setBaudrate(9600);
    serial::Timeout to = serial::Timeout::simpleTimeout(1000);
    ser.setTimeout(to);
    ser.open();
    
    if(ser.isOpen()){
        ROS_INFO_STREAM("Serial Port initialized");
    }
    else throw std::runtime_error("port is closed");
    
    sub = nh.subscribe(sub_topic, 100, &Pion_arduino_node::speeds_deliver_cb, this);
    pub = nh.advertise<geometry_msgs::Twist>(pub_topic, 100);
    
}

void Pion_arduino_node::speeds_deliver_cb(const geometry_msgs::Twist::ConstPtr& msg) {
    
    static float lin_prev = 0;
    static float ang_prev = 0;
    
    if (fabs(lin_prev - msg->linear.x) < speed_epsilon && fabs(ang_prev == msg->angular.z) < speed_epsilon)
        return;
    
    float speed_lin_left = msg->linear.x; // get linear (m/s) and angular (rad/s) speeds for robot
    float speed_ang_right = msg->angular.z;
    
    speeds_parser_to_ard(speed_lin_left,speed_ang_right);  // parse to left and right wheel speeds (rad/s)
    
    write_to_serial(speed_lin_left,speed_ang_right); // write to arduino
    
    lin_prev = msg->linear.x;
    ang_prev = msg->angular.z;
    
}

void Pion_arduino_node::check_serial() {
    
    if(ser.available()) {
        
        float speed_lin_left = 0;
        float speed_ang_right = 0;
        
        read_from_serial(speed_lin_left, speed_ang_right); // read left and right wheel speeds (in rad/s)
        
        speeds_parser_to_ros(speed_lin_left, speed_ang_right); // parse to linear (m/s) and angular (rad/s) robot speeds
        
        geometry_msgs::Twist msg;
        msg.linear.x = speed_lin_left;
        msg.angular.z = speed_ang_right;
        
        pub.publish(msg); // publish

    }
}

void Pion_arduino_node::write_to_serial(float left, float right) {
    pion.speeds[0] = left;
    pion.speeds[1] = right;
    
    ser.write(pion.transfer, 8);
    ROS_INFO_STREAM("Wrote to serial:\n left: " << pion.speeds[0] << "\nright: " << pion.speeds[1] << std::endl);
}

void Pion_arduino_node::read_from_serial(float &left, float &right) {
    
    ser.read(pion.transfer, 8);
    
    left = pion.speeds[0];
    right = pion.speeds[1];
    
    ROS_INFO_STREAM("Read from serial:\n left: " << left << "\nright: " << right << std::endl);
}

void Pion_arduino_node::speeds_parser_to_ard(float &lin, float &ang) {
    
    float lin_loc = lin;
    float ang_loc = ang;
    
    lin = (2 * lin_loc - ang_loc * base_width) / (2 * wheel_radius); // now it`s left wheel speed in rad/s
    ang = (2 * lin_loc + ang_loc * base_width) / (2 * wheel_radius); // now it`s right wheel speed in rad/s
    
}

void Pion_arduino_node::speeds_parser_to_ros(float &left, float &right) {
    float left_loc = left;
    float right_loc = right;
    
    left = 0.5 * wheel_radius * (right_loc + left_loc);
    right = wheel_radius * (right_loc - left_loc) / base_width;
}

int main(int argc, char* argv[]) {
    try {
        ros::init(argc, argv, "ser_test_node");
    
        Pion_arduino_node node("/dev/ttyUSB0", "/Pion/cmd_vel", "/Pion/odom_vel"); // from argv ?
    
        ros::Rate loop_rate(5);
    
        while (ros::ok()) {
            ros::spinOnce();
            node.check_serial();
            loop_rate.sleep();
        }
        ros::spinOnce();
    }
    catch(const std::exception &ex) {
        std::cerr << ex.what() << '\n';
        return -1;
    }
    catch(...) {
        std::cerr << "Smth really bad happend\n Luck!\n";
        return -2;
    }
    return 0;
}

