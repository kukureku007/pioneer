#include <math.h> // abs() 

union Pion_protocol {
  float speeds[2]; // [0] - left wheel, [1] - right wheel, + forward, - reverse, all speeds in rad/sec
  int   input[4];  // read form serial
  char  output[8]; // write to serial
};
//------------------------------------------------
//[BEG] PINS 

// plus pin to names

// Encoders pins !WARNING! DO NOT CHANGE INTERRUPT PINS
int encoder_A_left_pin  = 2; // interrupt 0 D2 
int encoder_B_left_pin  = 7; // D7

int encoder_A_right_pin = 3; // interrupt 1 D3
int encoder_B_right_pin = 8; // D8

// Engines pins 

int engine_forward_left_pin  = 5; // D5
int engine_reverse_left_pin  = 6; // D6

int engine_forward_right_pin = 9;  // D9
int engine_reverse_right_pin = 10; // D10

//[END] PINS
//------------------------------------------------
//[BEG] GLOBALS
const float pi2                     = 6.28318;
const byte  pwm_1rads               = 100; // Experimental value, pwm value for speed of 1rads, 100 is default
const int   encoder_full_revolution = 17000; // 17k - possible
const int   encoder_loop_skip       = 1000; // 1000 - possible, how much loop() iterations will be skip 
                                         // to make latency around 1.5sec between velocity check
const float rad_in_one_step = pi2 / encoder_full_revolution;
const byte  pwm_max         = 230; // max pwm, maybe 255 will be ok
const float speed_epsilon   = 1e-1; // maybe (max speed / max pwm)

Pion_protocol pion;

volatile long encoder_inc_left;
volatile long encoder_inc_right;

byte pwm_forvard_left;
byte pwm_reverse_left;
byte pwm_forvard_right;
byte pwm_reverse_right;

float speed_des_left;  // Desirable left speed from host
float speed_des_right; // Desirable right speed from host

float speed_real_left;  // Real left speed from encoder
float speed_real_right; // Real right speed from encoder

// unchecked 
long speed_last_check;

//[END] GLOBALS
//------------------------------------------------
//[BEG] FUNCTIONS

void speed_send_to_serial();// send 
void speed_set();           // set wheels w_speeds
void speed_check();         // get real speeds from encoders
inline bool speed_ok();
void speed_normalise();


//[BEG] Need to be updated, considering min_pwm
//void emergency_stop();

inline byte pwm_get(float vel);
inline byte pwm_get(float vel, float vel_real, float pwm);
//[END] Need to be updated, considering min_pwm

int pwm_change(byte loc_forward_left, byte loc_reverse_left, byte loc_forward_right, byte loc_reverse_right);

void encoder_inter_left();  // interrupt for left encoder
void encoder_inter_right(); // interrupt for right encoder

//[END] FUNCTIONS
//------------------------------------------------

void setup() {
  
  Serial.begin(9600);
  attachInterrupt(0, encoder_inter_left, CHANGE); // Initializing interrupts for D2 (0 inter) 
  attachInterrupt(1, encoder_inter_right, CHANGE); // D3 (1 inter)

  pinMode(encoder_B_left_pin, INPUT);
  pinMode(encoder_B_right_pin, INPUT);

  pinMode(engine_forward_left_pin, OUTPUT);
  pinMode(engine_reverse_left_pin, OUTPUT);
  pinMode(engine_forward_right_pin, OUTPUT);
  pinMode(engine_reverse_right_pin, OUTPUT);  

  pion.speeds[0]    = 0;
  pion.speeds[1]    = 0;
  encoder_inc_left  = 0;
  encoder_inc_right = 0;

  pwm_forvard_left  = 0;
  pwm_reverse_left  = 0;
  pwm_forvard_right = 0;
  pwm_reverse_right = 0; 

  speed_des_left  = 0; 
  speed_des_right = 0; 

  speed_real_left  = 0;
  speed_real_right = 0;

  speed_last_check = millis();
}

void loop() {
  static int encoder_check = 0;
  if ((encoder_check++) > encoder_loop_skip) {
    
    speed_check();
    speed_send_to_serial();

    if (!speed_ok())
      speed_normalise();
    
    encoder_check = 0;
  }
}

void serialEvent() { // reading from serial, each time before loop()
  if (Serial.available()) {
    int incoming;
    for (int i = 0; i < 4; ++i) {
      int incoming = Serial.read();
      pion.input[i] = incoming;
    }
    speed_set();   
  }
}

//------------------------------------------------
//Speed

void speed_set() { // set wheels w_speeds

//  if (speed_des_left == pion.speeds[0] && speed_des_right == pion.speeds[1]) 
//    return; // speeds still the same, now in ROS
 
  speed_des_left  = pion.speeds[0];
  speed_des_right = pion.speeds[1];
  
  byte loc_forward_left  = 0;
  byte loc_reverse_left  = 0;
  byte loc_forward_right = 0;
  byte loc_reverse_right = 0;

  if (speed_des_left > speed_epsilon) {
    loc_forward_left = pwm_get(speed_des_left);
  }
  else if (speed_des_left < -speed_epsilon) {
    loc_reverse_left = pwm_get(speed_des_left);
  }
  
  if (speed_des_right > speed_epsilon) {
    loc_forward_right = pwm_get(speed_des_right);
  }
  else if (speed_des_right < -speed_epsilon) {
    loc_reverse_right = pwm_get(speed_des_right);
  }

  pwm_change(loc_forward_left, loc_reverse_left, loc_forward_right, loc_reverse_right);

}

void speed_check() {
  
  long delta_time = millis() - speed_last_check;
  
  long loc_encoder_left = encoder_inc_left;
  long loc_encoder_right = encoder_inc_right;
  encoder_inc_left = 0;
  encoder_inc_right = 0;
  speed_last_check = millis();
    
  speed_real_left  = 1e3 * loc_encoder_left * rad_in_one_step / delta_time; // 1e3 for transfer to seconds from milliseconds(delta_time)
  speed_real_right = 1e3 * loc_encoder_right * rad_in_one_step / delta_time;

}

void speed_send_to_serial() {

  pion.speeds[0] = speed_real_left;
  pion.speeds[1] = speed_real_right;
  
  Serial.write(pion.output);
}

inline bool speed_ok() {
  return ((abs(speed_real_left - speed_des_left) < speed_epsilon) && (abs(speed_real_right - speed_des_right) < speed_epsilon));
}

void speed_normalise() {

  byte loc_forward_left  = 0;
  byte loc_reverse_left  = 0;
  byte loc_forward_right = 0;
  byte loc_reverse_right = 0;
  
  // if desirable and real speeds are in same direction, then we can increase or decrease pwm easily, if not - then stop [for now].
  if (speed_des_left * speed_real_left > 0 && speed_des_right * speed_real_right > 0) {
   
    if (speed_des_left > speed_epsilon) {
      loc_forward_left = pwm_get(speed_des_left, speed_real_left, pwm_forvard_left);
    }
    else if (speed_des_left < -speed_epsilon) {
      loc_reverse_left = pwm_get(speed_des_left, speed_real_left, pwm_reverse_left);
    }
    
    if (speed_des_right > speed_epsilon) {
      loc_forward_right = pwm_get(speed_des_right, speed_real_right, pwm_forvard_right);
    }
    else if (speed_des_right < -speed_epsilon) {
      loc_reverse_right = pwm_get(speed_des_right, speed_real_right, pwm_reverse_right);
    }
  }
  
  pwm_change(loc_forward_left, loc_reverse_left, loc_forward_right, loc_reverse_right);  
}

//------------------------------------------------
//PWM

inline byte pwm_get(float vel) {  // pwm_max ?? maybe too
  int pwm_new = pwm_1rads*abs(vel);
  return byte( (pwm_new > pwm_max) ? pwm_max : pwm_new );
}

inline byte pwm_get(float vel, float vel_real, float pwm) {
  int pwm_new = pwm*abs(vel/vel_real);
  return byte( (pwm_new > pwm_max) ? pwm_max : pwm_new );
}

int pwm_change(byte loc_forward_left, byte loc_reverse_left, byte loc_forward_right, byte loc_reverse_right) {

//  check that there is no reverse and forward pwm at the same time
  if ((loc_forward_left * loc_reverse_left != 0) || (loc_forward_right * loc_reverse_right != 0))
    return -1;
  
  // first, all pwm off
  analogWrite(engine_forward_left_pin, 0);
  analogWrite(engine_reverse_left_pin, 0);
  analogWrite(engine_forward_right_pin, 0);
  analogWrite(engine_reverse_right_pin, 0);

  // second, set new pwm 
  analogWrite(engine_forward_left_pin, loc_forward_left);
  analogWrite(engine_reverse_left_pin, loc_reverse_left);
  analogWrite(engine_forward_right_pin, loc_forward_right);
  analogWrite(engine_reverse_right_pin, loc_reverse_right);
  
  pwm_forvard_left  = loc_forward_left;
  pwm_reverse_left  = loc_reverse_left;
  pwm_forvard_right = loc_forward_right;
  pwm_reverse_right = loc_reverse_right; 

  return 0;
}

//------------------------------------------------
//Encoders

void encoder_inter_left() { // interrupt for left encoder
  if (digitalRead(encoder_A_left_pin) == digitalRead(encoder_B_left_pin)) 
    ++encoder_inc_left;
  else 
    --encoder_inc_left;
}

void encoder_inter_right() { // interrupt for right encoder
  if (digitalRead(encoder_A_right_pin) == digitalRead(encoder_B_right_pin)) 
    ++encoder_inc_right;
  else 
    --encoder_inc_right; 
}
