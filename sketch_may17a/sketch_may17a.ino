int engine_forward_left_pin  = 5; // D5
int engine_reverse_left_pin  = 6; // D6

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(engine_forward_left_pin, OUTPUT);
  pinMode(engine_reverse_left_pin, OUTPUT);
  
}

int i = 0;
byte start_pwm = 100;
byte max_pwm   = 200;

void loop() {
  // put your main code here, to run repeatedly:
  
  analogWrite(engine_forward_left_pin, i+start_pwm);

  if (i+start_pwm == max_pwm) {
    Serial.print(1);
  }
  else {
    ++i;
    Serial.println(i+start_pwm);
  }
  
  delay(1000);
}
